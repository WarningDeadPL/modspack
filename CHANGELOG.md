# Mods Pack 1.7.8
* Fixes
    - add models source from Blender 🤝 @mafoo
    - fixed Crushers models 🤝 @mafoo
    - fixed MK Blocks Display Names 🤝 @ad3d0
    
# Mods Pack 1.7.7
* Fixes
    - fixed malachit ore generate
    - removed the Cryo Chamber loc