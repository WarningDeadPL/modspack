@echo off

for %%a in (.) do set currentfolder=%%~na

if exist "bin" rd /q /s ".idea"

if exist "bin" rd /q /s "bin"

if exist "obj" rd /q /s "obj"

cd "..\SpaceEngineers\Bin64"

".\SEWorkshopTool.exe" --upload --compile --mods "%appdata%\SpaceEngineers\Mods\%currentfolder%" --exclude .csproj .sln .user .gitignore .bat --tags block

echo upload finished

timeout 10