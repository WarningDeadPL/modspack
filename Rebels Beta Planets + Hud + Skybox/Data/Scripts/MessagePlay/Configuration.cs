﻿/*
 *  Copyright (C) Chris Courson, 2016. All rights reserved.
 * 
 *  This file is part of MessagePlay, a Space Engineers mod available through Steam.
 *
 *  MessagePlay is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  MessagePlay is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with MessagePlay.  If not, see <http://www.gnu.org/licenses/>.
 */
using Sandbox.ModAPI;
using System.IO;
using System;

namespace MessagePlay
{
    public static class Configuration<T>
    {
        public static string ToXML(T obj)
        {
            return MyAPIGateway.Utilities.SerializeToXML(obj);
        }

        public static T FromXML(string xml)
        {
            return MyAPIGateway.Utilities.SerializeFromXML<T>(xml);
        }

        public static T LoadFromLocalStorage()
        {
            T obj;

            if (MyAPIGateway.Utilities.FileExistsInLocalStorage(typeof(T).Name + ".xml", typeof(MessagePlay)))
            {
                // WelcomePanel.xml exists so load it.
                TextReader textReader = MyAPIGateway.Utilities.ReadFileInLocalStorage(typeof(T).Name + ".xml", typeof(MessagePlay));
                string buffer = textReader.ReadToEnd();
                textReader.Close();
				try
				{
					obj = MyAPIGateway.Utilities.SerializeFromXML<T>(buffer);
				}
				catch (Exception ex)
				{
					MyAPIGateway.Utilities.ShowMessage("Exception", ex.Message);
					obj = default(T);
				}
            }
            else
            {
                obj = default(T);
            }

            return obj;
        }

        public static void CreateInLocalStorage(T obj)
        {
            string buffer = MyAPIGateway.Utilities.SerializeToXML(obj);
            TextWriter textWriter = MyAPIGateway.Utilities.WriteFileInLocalStorage(typeof(T).Name + ".xml", typeof(MessagePlay));
            textWriter.Write(buffer);
            textWriter.Close();
        }
    }
}
